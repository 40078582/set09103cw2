# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Contains the entire file structure of the python flask web application for running a lecture and workbook learning site.

* Version
1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Navigate to the /src folder and run "python __init__.py" which will launch the base application

* Configuration
All files to be used and displayed within the application should be added to the content_managment.py file dictionary in order to be displayed

* Dependencies
all dependencies listed and in the requirements file in the /src folder

* Database configuration
database uses sqlite3 and is contained within a singular file in the /src folder

### Who do I talk to? ###

* Repo owner or admin
40078582@live.napier.ac.uk

* Other community or team contact
module leader: simon.wells@live.napier.ac.uk