$(document).ready(function() {
	
    $(function() {
        $('#saveNotes').bind('click', function() {
            var notes = $('#textarea').val();
            var url = document.URL;
            var index = url.lastIndexOf("/")
            var file = url.substring(index, url.length)
            console.log(file)
            $.ajax({
                url: '/saveNotes/',
                data: JSON.stringify({
                    'note': notes,
                    'file': file
                }),
                contentType: 'application/json;charset=UTF-8',
                type: 'POST',
                success: function(response) {

                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });

    $(function() {
        $('#upload-button').bind('click', function() {
            var form_data = new FormData($('#upload-file')[0]);
            $.ajax({
                url: '/uploadNotes/',
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                asunc: false,
                type: 'POST',
                success: function(response) {
                    console.log('Success')
                    console.log(response['path'])
                    var path = response['path']
                    var notes = $('#textarea').val();
                    var url = document.URL;
                    var index = url.lastIndexOf("/")
                    var file = url.substring(index, url.length)
                    $.ajax({
                        url: '/addNotes/',
                        data: JSON.stringify({
                            'lecture': file,
                            'path': path
                        }),
                        contentType: 'application/json;charset=UTF-8',
                        type: 'POST',
                        success: function(response) {
                            console.log('Success')
                            console.log(response['path'])
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    })

                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });
	
	$(function() {
        $('#markComplete').bind('click', function() {
            var notes = $('#textarea').val();
            var url = document.URL;
            var index = url.lastIndexOf("/")
            var file = url.substring(index, url.length)
            $.ajax({
                url: '/update_progress/',
                data: JSON.stringify({'file':file}),
                contentType: 'application/json;charset=UTF-8',
                type: 'POST',
                success: function(response) {
					$('#progressSpan').toggleClass("glyphicon glyphicon-unchecked").toggleClass("glyphicon glyphicon-checked");
                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });

    $(function() {
        $('#searchBox').bind('input propertychange', function() {
			$('#searchResults').empty();
            var search = $('#searchBox').val();
			if(search.length>0)
			{
                $.ajax({
                    url: '/search/',
                    data: JSON.stringify(search),
                    contentType: 'application/json;charset=UTF-8',
                    type: 'POST',
                    success: function(response) {
						console.log(response)
						$('#searchResults').empty();
						$.each(response, function(k, v) {
							$('#searchResults').append('<li><a href="/lecture'+v+'" type="button" class="list-group-item">'+k+'</a></li>')
						});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
			}
        });
    });
	
	$(function() {
        $('.removeUser').bind('click', function() {
            var username = $(this).closest('tr').children('td.one').text();
			console.log(username)
            $.ajax({
                url: '/remove_user/',
                data: JSON.stringify(username),
                contentType: 'application/json;charset=UTF-8',
                type: 'POST',
                success: function(response) {
					console.log(response)
					location.reload();
                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });
	
	$(function() {
        $('.edit_button').bind('click', function() {
			id = $(this).closest('tr').children('td.zero').text();
            username = $(this).closest('tr').children('td.one').text();
			email = $(this).closest('tr').children('td.two').text();
			tut_group = $(this).closest('tr').children('td.three').text();
			rank = $(this).closest('tr').children('td.four').text();
			console.log(id);
			$('#editUsername').val(username);
			$('#editEmail').val(email);
			$('#editTut_group').val(tut_group);
			$('#editRank').val(rank);
			console.log(username)
        });
    });

	$(function() {
        $('#edit_submit').bind('click', function() {
            var username = $('#editUsername').val();
			var email = $('#editEmail').val();
			var tut_group = $('#editTut_group').val();
			var rank = $('#editRank').val();
            $.ajax({
                url: '/edit_user/',
                data: JSON.stringify({'id':id, 'username':username, 'email':email, 'tut_group':tut_group, 'rank':rank}),
                contentType: 'application/json;charset=UTF-8',
                type: 'POST',
                success: function(response) {
					console.log(response)
					location.reload();
                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });
	
	$(function() {
        $('#revert_button').bind('click', function() {
            $('#editUsername').val(username);
			$('#editEmail').val(email);
			$('#editTut_group').val(tut_group);
			$('#editRank').val(rank);
        });
    });
	
	$(function() {
        $('#share_button').bind('click', function() {
            var username = $('#shareUserSearch').val();
            $.ajax({
                url: '/check_users/',
                data: JSON.stringify(username),
                contentType: 'application/json;charset=UTF-8',
                type: 'POST',
                success: function(response) {
					if (response['success'] = true)
					{
						var notes = $('#textarea').val();
						var notes = $('#textarea').val();
						var url = document.URL;
						var index = url.lastIndexOf("/")
						var file = url.substring(index, url.length)
						$.ajax({
								url: '/share_notes/',
								data: JSON.stringify({'notes':notes, 'username':username, 'file':file}),
								contentType: 'application/json;charset=UTF-8',
								type: 'POST',
								success: function(response) {
									console.log(response)
								},
								error: function(error) {
									console.log(error);
								}
							})
					}
					else{
						console.log("fail")
					}
                },
                error: function(error) {
                    console.log(error);
                }
            })
        });
    });

});