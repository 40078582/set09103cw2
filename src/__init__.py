import ConfigParser
import logging
import gc
import os
import json
from datetime import datetime
from logging.handlers import RotatingFileHandler
from datetime import datetime
from flask import Flask, render_template, flash, request, redirect, url_for, session, jsonify
from content_management import content, admin
from dbconnect import connection
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from passlib.hash import sha256_crypt

topic_dict = content()
admin = admin()

app = Flask(__name__)


# checks if their is an active session
def checkSession():
  try:
    if(session['logged_in']):
      pass
      return True
  except KeyError:
    pass
    app.logger.info("invalid session")
    return False

@app.route('/')
def homepage():
    return redirect(url_for('dashboard'))

@app.route("/logout/")
def logout():
    session.clear()
    app.logger.info("session ended")
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('dashboard'))

@app.route('/login/', methods=['GET', 'POST'])
def login_page():
    error = ''
    try:
        c, conn = connection()
        if request.method == "POST":
            data = c.execute("SELECT * FROM users WHERE username = (?)", (request.form['username'],))
            data = c.fetchone()
            if not data:
                error = "Invalid credentials, please try again"
                app.logger.info("invalid credentials")
            if sha256_crypt.verify(request.form['password'], data[3]):
                session['logged_in'] = True
                session['username'] = request.form['username']
                flash("You are now logged in")
                return redirect(url_for("dashboard"))
            else:
                error = "Invalid Credentials, please try again"
            app.logger.info(str(request.form['username'])+" logged in")
        gc.collect()
        return render_template("login.html", error=error)
    except Exception as e:
        return render_template("login.html", error=str(e))

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4,max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [validators.Required(), validators.EqualTo('confirm', message="Passwords must match.")])
    confirm = PasswordField('Repeat Password')
    accept_tos=BooleanField('I accept the <a=href"/tos/">Terms of Service</a> and the <a href="/privacy/">Privacy Notice</a> (Last Updated in November 2015', [validators.Required()])
    
@app.route('/change_password/', methods=['GET', 'POST'])
def change_password_page():
  error = ""
  if checkSession() == True:
    if request.method == 'POST':
      try:
        username = session['username']
        oldPassword = request.form['oldPassword']
        newPassword = request.form['newPassword']
        confirmNewPassword = request.form['confirmNewPassword']
        c, conn = connection()
        data = c.execute("SELECT * FROM users WHERE username = (?)", (username,))
        data = c.fetchone()
        if data:
          if sha256_crypt.verify(oldPassword, data[3]):
            if newPassword == confirmNewPassword:
              password = sha256_crypt.encrypt(newPassword)
              c.execute("UPDATE users SET password = (?) WHERE username = (?)", (password,username,))
              conn.commit()
              c.close()
              gc.collect()
              app.logger.info(str(username)+" changed password at"+str(datetime.now()))
            else:
              error = "passwords do not match"
          else:
            error = "Incorrect old password"
        if error == "":
          flash("Password Changed")
          return redirect(url_for('account_page'))
        else:
          flash(error)
          return render_template("change_password.html")
      except Exception as e:
        print(e)
    else:
      return render_template("change_password.html")
  else:
    flash("You must log in to do that")
    return url_for('dashboard_page')

@app.route('/register/', methods=['GET', 'POST'])
def register_page():
    try:
        form = RegistrationForm(request.form)
        if request.method == "POST" and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.encrypt((str(form.password.data)))
            c, conn = connection()
            x = c.execute("SELECT * FROM users WHERE username = (?)", (username,))
            if c.fetchone():
                flash("That username is already taken, please choose another")
                return render_template('register.html', form=form)
            else:
                path = "static/userData/"
                for i in admin:
                  if i == username:
                    print("adding an admin")
                    rank = 'admin'
                    c.execute("INSERT INTO users (username, password, email, rank) VALUES (?,?,?,?)", (username, password, email, rank,))
                    conn.commit()
                  else:
                    print("adding a student")
                    rank = 'student'
                    c.execute("INSERT INTO users (username, password, email, rank) VALUES (?,?,?,?)", (username, password, email, rank,))
                    conn.commit()
                c.execute("SELECT id FROM users  WHERE username = (?)", (username,))
                data = c.fetchone()
                userID = data[0]
                flash("Thanks for registering")
                c.close()
                gc.collect()

                session['logged_in'] = True
                session['username'] = username

                userpath = os.path.join(path, username+"/")
                userNotes = os.path.join(str(userpath), "notes/")
                userTemp = os.path.join(str(userpath), "temp/")
                userTemp = os.path.join(str(userpath), "messages/")
                os.makedirs(userpath)
                os.makedirs(userNotes)
                os.makedirs(userTemp)
                userProgress = open(str(userpath)+"progress.txt", 'w').close()
                for i in topic_dict['Lectures']:
                  file = i[1].replace(".pdf",".txt")
                  file = file.replace("/","")
                  notes = os.path.join(str(userpath),"notes/"+file)
                  open(notes, 'a').close()
                return redirect(url_for('dashboard'))
                app.logger.info(username+" has registered")
        return render_template("register.html", form=form)
    except Exception as e:
        return (str(e))

@app.route('/dashboard/')
def dashboard():
  if checkSession() == True:
    tut_members = []
    final = []
    username = session['username']
    path = "static/userData/"+username+"/progress.txt"
    progress = ""
    division = 0
    percentage = 0
    completeCount = 0
    c, conn = connection()
    c.execute('SELECT tut_group FROM users WHERE username = (?)', (username,))
    data = c.fetchone()
    tut_group = data[0]
    print(tut_group)
    cursor = c.execute('SELECT username FROM users WHERE tut_group = (?)', (tut_group,))
    for key, value in topic_dict['Lectures']:
      temp = [key, value]
      final.append(temp)
    for group in cursor:
      tut_members.append(group[0])
    tut_members.sort()
    with open(path, 'r') as file:
      progress = file.read()
    for i in final:
      if len(i) != 3:
        if i[1] in progress:
          i.append("Complete")
          completeCount = completeCount + 1
        else:
          i.append("Not Complete")
      else:
        if i[1] in progress:
          i[2] = "Complete"
          completeCount = completeCount + 1
        else:
          i[2] = "Not Complete"
    print(len(final))
    print(completeCount)
    if completeCount > 0:
      divide = float(completeCount)/float(len(final))
      print("divide: "+str(divide))
      percentage = (100*divide)
      percentage = int(percentage)
    else:
      percentage = 0
    print(percentage)
    return render_template("dashboard.html", final=final, tut_members=tut_members, percentage=percentage)
  else:
    return render_template("dashboard.html", topic_dict=topic_dict)

@app.route('/account/', methods=['GET', 'POST'])
def account_page():
  if checkSession() == True:
    tut_group = ''
    username = session['username']
    try:
      if request.method == "POST":
        c, conn = connection()
        x = c.execute("UPDATE users SET tut_group = (?) WHERE username = (?)", (requestedTut_group, username,))
        conn.commit()
        flash("You have been register in "+requestedTut_group)
        c.close()
        gc.collect()
      else:
        c, conn = connection()
        x = c.execute("SELECT tut_group, rank FROM users WHERE username = (?)", (username,))
        data = c.fetchone()
        tut_group = data[0]
        accessLevel = data[1]
    except Exception as e:
        return str(e)
    return render_template("account.html", tut_group=tut_group, accessLevel=accessLevel)
  else:
    flash("You must log in first")
    return redirect(url_for('dashboard'))

@app.route('/saveNotes/', methods=['POST'])
def save_Notes():
  if checkSession() == True:
    username = session['username']
    notes = request.json['note']
    file = request.json['file']
    file = file.replace(".pdf",".txt")
    path = "static/userData/"+username+"/notes/"+file
    with open(path, 'w') as text_file:
      text_file.write(notes)
    print(notes)
    app.logger.info(username+" has saved notes for "+ str(file))
    return json.dumps({'status':'OK'});
  else:
    flash("You must log in first")
    return redirect(url_for(dashboard))

@app.route('/uploadNotes/', methods=['POST'])
def upload_notes():
  error = ''
  if checkSession() == True:
    username=session['username']
    if request.method == 'POST':
      file = request.files['datafile']
      app.logger.info(username+" attempting to upload file:"+file.filename)
      path = 'static/userData/'+username+'/temp/'+str(file.filename)
      print(path)
      if os.path.isfile(path):
        error='File:'+ file.filename +' already exists'
      if error=='':
        file.save(path)
        print(str(request.path))
        app.logger.info(username+" has uploaded notes to " + str(file))
    return jsonify({"success":True, "path":str(path)})
  else:
    flash("You must log in first")
    return redirect(url_for(dashboard))

@app.route('/search/', methods=['POST'])
def search():
  try:
    search = request.json
    print(search)
    response = []
    for i in topic_dict['Lectures']:
      if search.lower() in i[0].lower().strip():
        response.append(i)
    return jsonify(response)
  except Exception as e:
    print(str(e))

@app.route('/addNotes/', methods=['POST'])
def addNotes():
  if checkSession() == True:
    username = session['username']
    file = request.json['lecture']
    tempPath = request.json['path']
    lecture = file.replace(".pdf",".txt")
    path = "static/userData/"+username+"/notes/"+lecture
    with open(tempPath, 'r') as temp_file:
      data=temp_file.read()
    with open(path, 'a') as file:
      file.write("\n uploaded at "+str(datetime.now())+":\n"+data)
    os.remove(tempPath)
    return json.dumps({'status':'OK'})
  else:
    flash("You must log in first")
    return redirect(url_for(dashboard))


@app.route('/lecture/<chosenLecture>')
def lecture_page(chosenLecture):
  myNotes = ""
  nextLecture = ""
  lecture = chosenLecture.replace(".pdf", ".txt")
  count = 0
  sharedNotes = ""
  if checkSession() == True:
    username = session['username']
    if not os.path.isfile(str("static/userData/"+username+"/notes/"+lecture+".txt")):
      open(str("static/userData/"+username+"/notes/"+lecture+".txt"), 'w').close()
    with open(str("static/userData/"+username+"/notes/"+lecture)) as file:
      myNotes = file.read()
    print(str("static/userData/"+username+"/messages/"+lecture))
    if os.path.exists(str("static/userData/"+username+"/messages/"+lecture)):
      with open(str("static/userData/"+username+"/messages/"+lecture)) as file:
        sharedNotes = file.read()
    else:
      notes = ""
  for t in topic_dict["Lectures"]:
    if count+1 < len(topic_dict['Lectures']):
      if chosenLecture in t[1]:
        nextLecture = topic_dict['Lectures'][count+1][1]
      count = count + 1
  return render_template("lecture.html", chosenLecture=chosenLecture, myNotes=myNotes, nextLecture=nextLecture, sharedNotes=sharedNotes)

@app.route('/admin_management/')
def admin_management():
  if checkSession() == True:
    data = []
    c, conn = connection()
    c.execute("SELECT * FROM users")
    x = c.fetchall()
    for i in x:
      data.append(i)
    return render_template("admin.html", data=data)
  else:
    flash("You must log in first")
    return redirect(url_for('dashboard'))

@app.route('/remove_user/', methods=['POST'])
def remove_user():
  user = request.json
  c, conn = connection()
  c.execute("DELETE FROM users WHERE username = (?)", (user,))
  conn.commit()
  shutil.rmtree('static/userData/'+user)
  return jsonify({"success":True})

@app.route('/edit_user/', methods=['POST'])
def edit_user():
  uid = request.json['id']
  username = request.json['username']
  email = request.json['email']
  tut_group = request.json['tut_group']
  rank = request.json['rank']
  c, conn = connection()
  c.execute("UPDATE users SET username = (?), email = (?), tut_group = (?), rank = (?) WHERE id = (?)", (username, email, tut_group, rank, uid,))
  conn.commit()
  app.logger.info("user with id: "+ uid+" has been changed to "+str(username)+" "+str(email)+" "+str(tut_group))
  return jsonify({"success":True})

@app.route('/update_progress/', methods=['POST'])
def update_progress():
  if checkSession() == True:
    username = session['username']
    file = request.json['file']
    path = "static/userData/"+username+"/progress.txt"
    count = 0
    if not file in open(path).read():
      with open(path, 'a') as progress:
        progress.write(str(file)+"\n")
    return jsonify({"success":True})

@app.route('/get_users/', methods=['POST'])
def get_users():
  if checkSession() == True:
    userCheck = request.json
    c, conn = connection()
    c.execute("SELECT username FROM users")
    data = c.fetchall();
    if any(userCheck in s for s in data):
      return jsonify({"success":True})
    else:
      return jsonify({"failure":True})

@app.route('/share_notes/', methods=['POST'])
def share_notes():
  if checkSession() == True:
    notes = request.json['notes']
    sender = session['username']
    file = request.json['file']
    file = file.replace(".pdf",".txt")
    recipient = request.json['username']
    c, conn = connection()
    c.execute("SELECT username FROM users WHERE username = (?)", (recipient,))
    result = c.fetchone()
    path = "static/userData/"+recipient+"/messages/"+file
    time = datetime
    with open(path, "a") as sharefile:
      sharefile.write("shared by "+sender+" at "+str(datetime.now())+"\n"+notes+"\n\n")
    return jsonify({"success":True})
    
  

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")


@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html", error=e)


@app.errorhandler(405)
def method_not_allowed(e):
    return render_template("500.html", error=e)

@app.route('/config/')
def config():
    str = []
    str.append('Debug:'+app.config['DEBUG'])
    str.append('port:'+app.config['port'])
    str.append('url:'+app.config['url'])
    str.append('ip_address:'+app.config['ip_address'])
    str.append('allowed_extensions'+app.config['allowed_extensions'])
    return '\t'.join(str)

def init(app):
    config = ConfigParser.ConfigParser()
    try:
        config_location = "etc/defaults.cfg"
        config.read(config_location)
        app.config['DEBUG'] = config.get("config", "debug")
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port")
        app.config['url'] = config.get("config", "url")
        app.config['log_file']=config.get("logging","name")
        app.config['log_location']=config.get("logging","location")
        app.config['log_level']=config.get("logging","level")
        app.secret_key=config.get("config","secret_key")
    except Exception as e:
        print "Could not read config file: ", config_location, str(e)

def logs(app):
    log_pathname = app.config['log_location']+app.config['log_file']
    file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024*10, backupCount=1024)
    file_handler.setLevel(app.config['log_level'])
    formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
    file_handler.setFormatter(formatter)
    app.logger.setLevel(app.config['log_level'])
    app.logger.addHandler(file_handler)

if __name__ == "__main__":
    init(app)
    logs(app)
    app.run(
      host=app.config['ip_address'],port=int(app.config['port']))
