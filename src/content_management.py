import os

def content():
    topic_dict = {"Lectures":[
                            ["Module Overview", "/L00_overview.pdf"],
                            ["Intro & Tools", "/L01_intro+tools.pdf"],
                            ["HTTP", "/L02_http.pdf"],
                            ["APIs, Web Services and REST", "/L03_apis+web.services+rest.pdf"],
                            ["Data Transports (JSON & XML)", "/L04_data.transport.pdf"],
                            ["Architectures for Scalability", "/L06_architectures.pdf"],
                            ["Security and Privacy", "/L07_Security+Privacy.docx"],
                            ["The Semantic Web", "/L08_semantic.pdf"],
                            ["The Realtime Web", "/L09_realtime.pdf"],
                            ["The Dark Web", "/L10_dark.pdf"],
                            ]}
    return topic_dict

def admin():
    admin = ["robjack"]
    return admin
            
