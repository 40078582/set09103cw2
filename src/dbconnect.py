import sqlite3 as lite
import sys

def connection():
    sys.setrecursionlimit(10000)
    conn = lite.connect('database.db')
    c = conn.cursor()
    return c, conn
